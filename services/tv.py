from models.media_item import MediaItemModel, MediaType
from sqlalchemy.orm.session import Session


class TVService():
    def __init__(self, db: Session) -> None:
        self.db = db

    def get_tvshows(self):
        result = self.db.query(MediaItemModel).filter_by(
            media_type=MediaType.TV).all()
        return result

    def get_tvshow_data(self, id: int):
        result = self.db.query(MediaItemModel).filter_by(
            media_type=MediaType.TV, id=id).first()
        return result

    def get_tvshows_by_category(self, category: str):
        result = self.db.query(MediaItemModel).filter_by(
            media_type=MediaType.TV, category=category).all()
        return result
