from models.media_item import MediaItemModel, MediaType
from sqlalchemy.orm.session import Session


class MovieService():
    def __init__(self, db: Session) -> None:
        self.db = db

    def get_movies(self):
        result = self.db.query(MediaItemModel).filter_by(
            media_type=MediaType.MOVIE).all()
        return result

    def get_movie_data(self, id: int):
        result = self.db.query(MediaItemModel).filter_by(
            media_type=MediaType.MOVIE, id=id).first()
        return result

    def get_movies_by_category(self, category: str):
        result = self.db.query(MediaItemModel).filter_by(
            media_type=MediaType.MOVIE, category=category).all()
        return result
