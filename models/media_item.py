from pydantic import BaseModel, Field
from typing import Optional
from datetime import datetime
from enum import Enum

from config.database import Base
from sqlalchemy import Column, Integer, String, Enum as SQLEnum
from pydantic_sqlalchemy import sqlalchemy_to_pydantic


class MediaType(str, Enum):
    MOVIE = 'movie'
    TV = 'tv'


class MediaItemModel(Base):
    __tablename__ = "MediaItem"

    id = Column(Integer, primary_key=True)
    title = Column(String)
    synopsis = Column(String)
    media_type = Column(SQLEnum(MediaType))
    year = Column(Integer)
    category = Column(String)


Pydantic_MediaItemModel = sqlalchemy_to_pydantic(MediaItemModel)


class MediaItem(BaseModel):
    id: Optional[int] = None
    title: str = Field(max_length=100)
    synopsis: str = Field(max_length=1000)
    media_type: Optional[MediaType]
    year: int = Field(le=datetime.now().year)
    category: Optional[str] = Field(max_length=50)

    class Config:
        schema_extra = {
            "example": {
                "id": 0,
                "title": "Title",
                "synopsis": "Short description",
                "year": datetime.now().year,
                "category": "action",
            }
        }
