from fastapi import FastAPI
from fastapi.responses import HTMLResponse

from config.database import engine, Base
from middlewares.jwt_bearer import JWTBearer
from middlewares.error_handler import ErrorHandler

from routers import movie_router, tv_router, user_router


app = FastAPI()
app.title = 'First app with FastAPI'
app.version = '0.0.1'

app.add_middleware(ErrorHandler)

# MOVIES ENDPOINTS
app.include_router(movie_router)

# TV ENDPOINTS
app.include_router(tv_router)

# USER ENDPOINTS
app.include_router(user_router)

Base.metadata.create_all(bind=engine)


@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>Hola mundo que tal</h1>')
