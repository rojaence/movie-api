from .movie import movie_router
from .tv import tv_router
from .user import user_router
