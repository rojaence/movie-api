from fastapi import APIRouter
from fastapi import HTTPException, Path, Query
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from typing import List
from models.media_item import MediaItem, MediaType, MediaItemModel, Pydantic_MediaItemModel

from config.database import Session
from services import TVService

tv_router = APIRouter()


@tv_router.get('/tv', tags=['tv'], response_model=List[Pydantic_MediaItemModel], status_code=200)
async def get_tvshows() -> List[Pydantic_MediaItemModel]:
    db = Session()
    tvshows = TVService(db).get_tvshows()
    return JSONResponse(status_code=200, content=jsonable_encoder(tvshows))


@tv_router.get('/tv/{id}', tags=['tv'], response_model=Pydantic_MediaItemModel, status_code=200)
async def get_tv_data(id: int = Path(ge=1)) -> Pydantic_MediaItemModel:
    db = Session()
    tvshow = TVService(db).get_tvshow_data(id)
    if not tvshow:
        raise HTTPException(status_code=404, detail="Not found")
    return JSONResponse(status_code=200, content=jsonable_encoder(tvshow))


@tv_router.get('/tv/', tags=['tv'], response_model=List[Pydantic_MediaItemModel], status_code=200)
async def get_tvshows_by_category(category: str = Query(min_length=1, max_length=20)) -> List[Pydantic_MediaItemModel]:
    db = Session()
    tvshows = TVService(db).get_tvshows_by_category(category)
    return JSONResponse(status_code=200, content=jsonable_encoder(tvshows))


@tv_router.post('/tv', tags=['tv'], status_code=201)
async def create_tvshow(data: MediaItem):
    db = Session()
    new_tvshow = MediaItemModel(**data.dict())
    new_tvshow.media_type = MediaType.TV
    db.add(new_tvshow)
    db.commit()
    return {"message": "It has been added successfully"}


@tv_router.put('/tv/{id}', tags=['tv'], status_code=200)
async def update_tv(id: int, data: MediaItem):
    db = Session()
    tvshow = db.query(MediaItemModel).filter_by(
        media_type=MediaType.TV, id=id).first()
    if not tvshow:
        raise HTTPException(status_code=404, detail="Id not found")
    tvshow.title = data.title
    tvshow.synopsis = data.synopsis
    tvshow.year = data.year
    tvshow.category = data.category
    db.commit()
    return {"message": "It has been updated successfully"}


@tv_router.delete('/tv/{id}', tags=['tv'], status_code=200)
async def delete_tv(id: int):
    db = Session()
    tvshow = db.query(MediaItemModel).filter_by(
        media_type=MediaType.TV, id=id).first()
    if not tvshow:
        raise HTTPException(status_code=404, detail="Id not found")
    db.delete(tvshow)
    db.commit()
    return {"message": "It has been deleted successfully"}
