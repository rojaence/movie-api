from fastapi import APIRouter
from fastapi import Depends, HTTPException, Path, Query
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from typing import List
from models.media_item import MediaItem, MediaType, MediaItemModel, Pydantic_MediaItemModel

from config.database import Session
from middlewares.jwt_bearer import JWTBearer
from services import MovieService

movie_router = APIRouter()


@movie_router.get('/movies', tags=['movies'], response_model=List[Pydantic_MediaItemModel], status_code=200, dependencies=[Depends(JWTBearer())])
async def get_movies() -> List[Pydantic_MediaItemModel]:
    db = Session()
    movies = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(movies))


@movie_router.get('/movies/{id}', tags=['movies'], response_model=Pydantic_MediaItemModel, status_code=200)
async def get_movie_data(id: int = Path(ge=1)) -> Pydantic_MediaItemModel:
    db = Session()
    data = MovieService(db).get_movie_data(id)
    if not data:
        raise HTTPException(status_code=404, detail="Not found")
    return JSONResponse(status_code=200, content=jsonable_encoder(data))


@movie_router.get('/movies/', tags=['movies'], response_model=List[Pydantic_MediaItemModel], status_code=200)
async def get_movies_by_category(category: str = Query(min_length=1, max_length=20)) -> List[Pydantic_MediaItemModel]:
    db = Session()
    movies = MovieService(db).get_movies_by_category(category)
    return JSONResponse(status_code=200, content=jsonable_encoder(movies))


@movie_router.post('/movies', tags=['movies'], status_code=201)
async def create_movie(data: MediaItem):
    db = Session()
    new_movie = MediaItemModel(**data.dict())
    new_movie.media_type = MediaType.MOVIE
    db.add(new_movie)
    db.commit()
    return {"message": "It has been added successfully"}


@movie_router.put('/movies/{id}', tags=['movies'])
async def update_movie(id: int, data: MediaItem):
    db = Session()
    movie = db.query(MediaItemModel).filter_by(
        media_type=MediaType.MOVIE, id=id).first()
    if not movie:
        raise HTTPException(status_code=404, detail="Id not found")
    movie.title = data.title
    movie.synopsis = data.synopsis
    movie.year = data.year
    movie.category = data.category
    db.commit()
    return {"message": "It has been updated successfully"}


@movie_router.delete('/movies/{id}', tags=['movies'])
async def delete_movie(id: int):
    db = Session()
    movie = db.query(MediaItemModel).filter_by(
        media_type=MediaType.MOVIE, id=id).first()
    if not movie:
        raise HTTPException(status_code=404, detail="Id not found")
    db.delete(movie)
    db.commit()
    return {"message": "It has been deleted successfully"}
